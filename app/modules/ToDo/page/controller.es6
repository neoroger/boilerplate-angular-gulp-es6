
angular.module('app.ToDo').controller('controller', ($scope) =>
{
    $scope.list = ['a', 'b'];

    $scope.add = (item) => {
        $scope.list.push(item);
        $scope.newItem = '';
    }

});