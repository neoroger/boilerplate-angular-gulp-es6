'use strict';

angular.module('app.ToDo').controller('controller', function ($scope) {
    $scope.list = ['a', 'b'];

    $scope.add = function (item) {
        $scope.list.push(item);
        $scope.newItem = '';
    };
});

//# sourceMappingURL=controller.js.map