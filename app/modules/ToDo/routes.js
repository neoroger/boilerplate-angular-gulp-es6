'use strict';

angular.module('app.ToDo').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('todo', {
        url: "/ToDo",
        templateUrl: 'modules/ToDo/page/template.html',
        controller: 'controller'
    });
});

//# sourceMappingURL=routes.js.map