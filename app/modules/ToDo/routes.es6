
angular.module('app.ToDo').config(($stateProvider, $urlRouterProvider) =>
{
    $stateProvider
        .state('todo', {
            url: "/ToDo",
            templateUrl: 'modules/ToDo/page/template.html',
            controller: 'controller'
        });
});