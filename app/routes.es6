
angular.module('app').config(($stateProvider, $urlRouterProvider) =>
{
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: 'pages/home/home.template.html',
            controller: 'HomeCtrl'
        })
        .state('bootsdoc', {
            url: "/bootsdoc",
            templateUrl: 'pages/bootsdoc/bootsdoc.template.html',
            controller: 'BootsdocCtrl'
        })
        .state('foods', {
            url: "/foods",
            templateUrl: 'pages/foods/foods.template.html',
            controller: 'FoodsCtrl'
        })
        .state('foods.detail', {
            url: "/:id",
            templateUrl: 'pages/foods/detail/foodsDetail.template.html',
            controller: 'FoodsDetailCtrl'
        })
});