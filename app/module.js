angular.module("app", ["ui.router","ui.bootstrap","angular-storage","ngAnimate","app.ToDo"])

.constant("name", "app")

.constant("deps", [
	"ui.router",
	"ui.bootstrap",
	"angular-storage",
	"ngAnimate",
	"app.ToDo"
])

.constant("wrap", false)

;