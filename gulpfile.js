

/*----------------------------------------------*\
 TASKS
 \*----------------------------------------------*/

/*
 # DEV
 - gulp dev          : Injecta las dependencias de bower en index.jade (y lo compila en html)
 - gulp serve        : Crea un servidor web y publica la carpeta ./app

 # BUILD
 - gulp build        : Compila y minifica todos los archivos y los injecta en ./dist/index.jade (y lo compila)
 - gulp serve-dist   : Crea un servidor web y publica la carpeta ./dist
 */




/*----------------------------------------------*\
 VARS
 \*----------------------------------------------*/


// Core
var gulp    = require('gulp');
var fs      = require('fs');
var path    = require('path');
var es      = require('event-stream');              // Concatena streams
var series  = require('stream-series');             // Idem pero en serie
var connect = require('gulp-connect');              // Crea servidor web

// Min & Ugly
var minifyCSS  = require('gulp-minify-css');        // CSS
var minifyHTML = require('gulp-minify-html');       // HTML
var uglify     = require('gulp-uglify');            // JS
var sourcemaps = require('gulp-sourcemaps');

// Utils
var filter      = require('gulp-filter');            // Filtra un stream
var concat      = require('gulp-concat');            // Concatena un stream en un solo fichero
var inject      = require('gulp-inject');            // Injecta un stream en un archivo html, jade, etc
var gulpsync    = require('gulp-sync')(gulp);        // Ejecuta tareas en serie
var jade        = require('gulp-jade');              // Compila jade
var replace     = require('gulp-replace');           // Modifica strings
var jeditor     = require("gulp-json-editor");       // Permite ver y editar json
var babel       = require('gulp-babel');             // ES6 to ES5
var ngConstant  = require('gulp-ng-constant');       // Prepara el archivo MODULE según un json de configuración
var rename      = require("gulp-rename");            // Renombra archivos

// Bower
var bowerFiles           = require('main-bower-files');             // Obtiene todas las dependencias de bower
var angularTemplatecache = require('gulp-angular-templatecache');   // Cachea todos los .html en un fichero

// Debug
var print = require('gulp-print');                  // Hace consolelog de un stream



/*----------------------------------------------*\
                    UTILS
 \*----------------------------------------------*/

function getFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function(file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}

/*----------------------------------------------*\
 Inject & Build
 \*----------------------------------------------*/

/**
 * Genera las dependencias necesarias para development
 */
gulp.task('dev', ['prepare-bower'], function () {

    var bowerOpts       = {
        paths: {
            bowerDirectory: './bower_components',
            bowerrc:        './.bowerrc',
            bowerJson:      './bower.json'
        }
    };
    var vendor    = gulp.src(bowerFiles(bowerOpts));
    var appModule = gulp.src(['./app/module.js']);
    var appRoutes = gulp.src(['./app/routes.js']);
    var appJS     = gulp.src(['./app/**/*.js', '!./app/module.js', '!./app/routes.js', '!./app/modules/**/*.*']);
    var appCSS    = gulp.src(['./app/**/*.css', '!./app/modules/**/*.*']);

    var modulesFiles = [];
    getFolders('./app/modules').forEach(function(folder)
    {
        var baseFolder = './app/modules/'+folder;
        var module  = gulp.src([baseFolder+'/module.js']);
        var routes  = gulp.src([baseFolder+'/routes.js']);
        var files   = gulp.src([baseFolder+'/**/*.js',baseFolder+'/**/*.css','!'+baseFolder+'/module.js','!'+baseFolder+'/routes.js']);
        modulesFiles.push(module);
        modulesFiles.push(files);
        modulesFiles.push(routes);
    });

    return gulp.src('./app/index.jade')
        .pipe(inject(vendor,                                        { name: 'vendor',   relative : true }))
        .pipe(inject(series(modulesFiles),                          { name: 'modules',  relative : true }))
        .pipe(inject(series(appCSS, appModule, appJS, appRoutes),   { name: 'app', relative: true }))
        //.pipe(inject(series(appModule, appJS, appRoutes),   { name: 'app',      relative : true }))
        .pipe(gulp.dest('./app'))
        .pipe(jade())
        .pipe(gulp.dest('./app'));
});


gulp.task('load-config', function()
{
    return gulp.src('./config.json')
        .pipe(ngConstant({}))
        .pipe(rename('module.js'))
        .pipe(gulp.dest('./app'));
});


/**
 * Crea el directorio /dist con lo necesario para distribuir
 */
gulp.task('build', ['prepare-bower'], function () {

    var bowerOpts       = {
        paths: {
            bowerDirectory: './bower_components',
            bowerrc:        './.bowerrc',
            bowerJson:      './bower.json'
        }
    };
    var vendor = gulp.src(bowerFiles(bowerOpts));
    var vendorJS = vendor
        .pipe(filter('**/*.js'))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(uglify({mangle:false}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js'));
    var vendorCSS = vendor
        .pipe(filter('**/*.css'))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.css'))
        .pipe(minifyCSS({comments:false,spare:true}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/styles'));
    var appModule = gulp.src(['./app/core/module.js'])
        .pipe(sourcemaps.init())
        .pipe(uglify({mangle:false}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js'));
    var appRoutes = gulp.src(['./app/core/routes.js'])
        //.pipe(sourcemaps.init())
        .pipe(uglify({mangle:false}))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js'));
    var appJS = gulp.src(['./app/**/*.js', '!./app/core/module.js', '!./app/core/routes.js'])
        .pipe(sourcemaps.write())
        .pipe(concat('scripts.js'))
        .pipe(uglify({mangle:false}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js'));
    var appCSS = gulp.src(['./app/**/*.css'])
        .pipe(sourcemaps.write())
        .pipe(concat('styles.css'))
        .pipe(minifyCSS({comments:false,spare:true, processImport:false}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/styles'));
    var templateCache = gulp.src(['./app/**/*.html', '!./app/index.html'])
        .pipe(angularTemplatecache('templates.js', { module : 'app'}))
        .pipe(gulp.dest('./dist/js'));
    var images = gulp.src(['./app/styles/img/*.*'])
        .pipe(gulp.dest('./dist/styles/img'));


    //*** Movemos las fuentes

    gulp.src(['./bower_components/bootstrap/fonts/*.*', './bower_components/font-awesome/fonts/*.*'])
        .pipe(gulp.dest('./dist/fonts'));


    //***** Cache breaker añadiendo timestamp

    if (!Date.now)
        Date.now = function() { return new Date().getTime(); };
    var timestamp = Math.floor(Date.now() / 1000);
    var funcTransform = function (filepath){
        var extension = filepath.split('.').pop();
        switch(extension)
        {
            //case 'js':  return '<script src="'+filepath+'?_='+timestamp+'"></script>';
            //case 'css':  return '<link rel="stylesheet" href="'+filepath+'?_='+timestamp+'">';
            case 'js':  return 'script(src="'+filepath+'?_='+timestamp+'")';
            case 'css':  return 'link(rel="stylesheet", href="'+filepath+'?_='+timestamp+'")';
            default: return '';
        }
    };


    return gulp.src('./app/index.jade')
        .pipe(inject(es.merge(vendorCSS, vendorJS), {name: 'vendor', transform: funcTransform, ignorePath : 'dist', addRootSlash : false }))
        .pipe(inject(appCSS, { name: 'app', transform: funcTransform, ignorePath : 'dist', addRootSlash : false }))
        .pipe(inject(series(appModule, templateCache, appJS, appRoutes), { name: 'app', transform: funcTransform, ignorePath : 'dist', addRootSlash : false}))
        //.pipe(gulp.dest('dist'))
        .pipe(jade())
        .pipe(minifyHTML())
        .pipe(gulp.dest('dist'));
});


/*----------------------------------------------*\
 UTILS
 \*----------------------------------------------*/

/**
 * Adapta las librerías para poderlas importar
 */
gulp.task('prepare-bower', function () {

    // Font-awesome no tiene en el main la versión en CSS. Se la ponemos.
    var fontAwesome = gulp.src(['./bower_components/font-awesome/.bower.json'])
        .pipe(jeditor(function(json)
        {
            if(json.main.indexOf('css/font-awesome.css') < 0)
                json.main.push('css/font-awesome.css');
            return json;
        }))
        .pipe(gulp.dest('./bower_components/font-awesome/'));

    // Bootstrap no tiene en el main la versión en CSS. Se la ponemos.
    var bootstrap = gulp.src(['./bower_components/bootstrap/.bower.json'])
        .pipe(jeditor(function(json)
        {
            if(json.main.indexOf('dist/css/bootstrap.css') < 0)
                json.main.push('dist/css/bootstrap.css');
            return json;
        }))
        .pipe(gulp.dest('./bower_components/bootstrap/'));
});


/*----------------------------------------------*\
 SERVER
 \*----------------------------------------------*/

/**
 * Crea un servidor http en local
 */
gulp.task('serve', function () {
    connect.server({
        root: ['app/', '.'],
        port: 1111
    });
});
gulp.task('serve-dist', function () {
    connect.server({
        root: ['dist/', '.'],
        port: 1110
    });
});


/*----------------------------------------------*\
 TASKs Shortcuts
 \*----------------------------------------------*/

/**
 * Tarea por defecto
 */
gulp.task('default', gulpsync.sync(['dev', 'serve']));

gulp.task('dist', gulpsync.sync(['build', 'serve-dist']));